# termss

A terminal screensaver to replace xscreensaver & Co.  
Written in bash.

Includes:

- the `termss` wrapper script: It will open a fullscreen terminal window on top of everything (**urxvt** is a hard dependency because it has the options to make this work as desired) and execute a command given as an argument, inside that terminal. Then it starts listening to events, and will kill everything as soon as you wiggle your mouse pointer or hit a key. Just like any screensaver. No screen locking.
- a script (`marquee1`) designed to run inside that window. It shows configurable command output (by default in awesome `toilet` fonts) in random colors and position, waits, clears the screen and starts again.
It's possible to have some sort of animation ("teletype effect"), and it can be made to sprinkle the screen with stars. Enable the options in marquee1.conf.

## Prerequisites

The Xorg server has a screensaver built in; its settings can be queried via `xset`.  
Some programs have the capability to catch the Xorg server's signal that it's time, and activate a custom screensaver or any other command - in our case `termss`.

[xss][1] is one such program, and it is the one I'm using. See "The scripts" below for examples.

[xss-lock][4] is another, and so is [xautolock][5] (this link for FreeBSD's
Freshports; look under "Master Sites" to find sources).

Essentially these three programs do the same thing: They provide a mechanism to
start an external application when the X server says "no user activity, time to
start the screensaver".

## Configuration

Both scripts look for its config file which can be put in `$XDG_CONFIG_HOME/termss/` (usually `~/.config/termss/`. Config files are named after the script + `.conf`, so if you rename your script to e.g. my_termss, i t looks for `my_termss.conf`.

Two example config files with default options can be found in the `config` subfolder of this repository.

## Dependencies

### The termss script

- Xorg
- `bash`
- [urxvt][2] terminal emulator
- [xdotool][10]
- `xrandr`, possibly part of `xorg-xrandr`
- `xinput`, possibly part of `xorg-xinput`
- `sed`
- a writeable `$TMPDIR` (defaults to `/tmp`)

#### Optional

- [hhpc][11] or [unclutter][uc] to hide the pointer
- (for insecure ungrabbing & pointer hiding) `setxkbmap`, possibly part of `xorg-setxkbmap`
- (for the screencast option) `imagemagick` and `gifsicle`
- to STOP/CONTinue processes when termss starts/stops: `pidof` (procps-ng)

#### Fonts

See `man urxvt` on how to formulate a font string for urxvt's "-fn" option, to be supplied as `font="..."` in `termss.conf`.

The `termss` script needs to detect the width & height of one monospaced cell, i.e. the space any character uses.  
There is automatic detection but it is tricky: it flashes a 1x1 sized terminal for just as long as it takes to determine its actual size. To avoid doing this every time the screensaver fires up, the script modifies its config file to add two values, `charwidth` and `charheight`.
Remember to remove these if you change fonts.

### The marquee1 script

- `ncurses` to provide `tput`
- `shuf` and `od` (both coreutils)
- `grep`

#### Optional

- [pv][14] to slow down output (teletype effect)
- [toilet][7] to create banners - ___highly___ recommended!
- [mpi-panel][12] for media player info (also from [here][13])

## The scripts

It isn't hard to imagine all the programs one can run in fullscreen to make for
a nice screensaver, but this repository uses a terminal emulator.

One could also run [pipes](http://acidhub.github.io/pipes.sh) for example, or [asciiquarium](https://robobunny.com/projects/asciiquarium/html/) or [termsaver][ts]... 

You will need a command like this in your `autostart`:

    xss /path/to/termss /path/to/marquee1 &
    # or
    xss /path/to/termss asciiquarium &
    # or
	xss /path/to/termss sh -c 'cmatrix|lolcat'
    # or
	xss /path/to/termss sh -c 'tput civis; termsaver matrix'
	# etc.

## Other

If you use **dunst** as your notification daemon, `termss` will pause dunst while the screensaver is active. If you have dunst in your `$PATH`, and it is not running, it will be started first. Set `dunst=0` in `termss.conf` to avoid this.

## Disclaimers

There's not enough quality control. Set it up carefully.  
But once it works, it works well enough. Stable, one could say.

There are some situations where the mouse pointer cannot be hidden because some application _grabbed_ it, so it should be _ungrabbed_ first.
However, ungrabbing needs to be enabled first because it is [potentially less secure](http://who-t.blogspot.fr/2012/01/xkb-breaking-grabs-cve-2012-0064.html).
You can set `insecure=1` inside the script, which uses an alternative method that is good enough for most cases, and ungrabbing stays disabled.
But you should know that this option then stays enabled for the remainder of that X session.  

[1]: https://github.com/9wm/xss
[2]: http://software.schmorp.de/pkg/rxvt-unicode.html
[3]: https://github.com/aktau/hhpc.git
[4]: https://bitbucket.org/raymonad/xss-lock
[5]: https://www.freshports.org/x11/xautolock/
[6]: http://www.figlet.org/
[7]: http://caca.zoy.org/wiki/toilet
[9]: https://dunst-project.org/
[10]: http://www.semicomplete.com/projects/xdotool/
[11]: https://github.com/aktau/hhpc
[12]: https://notabug.org/ohnonot/media-player-info
[13]: https://framagit.org/ohnonot/media-player-info
[14]: http://www.ivarch.com/programs/pv.shtml
[ts]: https://github.com/brunobraga/termsaver/
[uc]: https://github.com/Airblader/unclutter-xfixes
